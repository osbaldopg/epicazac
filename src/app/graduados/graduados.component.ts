import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { empty } from 'rxjs';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-graduados',
  templateUrl: './graduados.component.html',
  styleUrls: ['./graduados.component.css']
})
export class GraduadosComponent implements OnInit {

  codigoUrl: {codigo: string};
  encontrado =0;
  nombre = "";
  invitados = [""];
  mesa = 0;
  horario = "";

  graduados = [
    {
      codigo : "1",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Dolores Carillo"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "2",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Gustavo Carrillo "],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "3",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Jose Manuel Carlos"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "4",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Luz María Félix"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "5",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Alejandro Carlos"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "6",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Daniel Carlos"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "7",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Estela López"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "8",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Estela Acuña"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "9",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Melisa Acuña"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "10",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Oscar Acuña"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "11",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Miguel Casanova"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "12",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Francisco Carrillo Carlos"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "13",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Gerardo Martínez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "14",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Fernando Argüelles"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "15",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Rafael Rivera"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "16",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Gaby González"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "17",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Sergio Aguilera"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "18",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Humberto de la Torr"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "19",
      nombre   : "Francisco Carrillo Carlos",
      invitados: ["Imanol Huerta"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "20",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Raúl Botello Lazalde"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "21",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Adela Sánchez Armenta"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "22",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Maria de los Angeles Hurtado"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "23",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Javier Valadez Becerra"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "24",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Elmer Botello Lazalde "],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "25",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Leonila Hurtado Cabral"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "26",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Isaias Torres Hurtado"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "27",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Susana Escobedo"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "28",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Noe Botello Lazalde"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "29",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Ana Karla Botello"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "30",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Karen Maldonado"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "31",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Andrea Botello"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "32",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Natalia Miramontes"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "33",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Leonel Botello"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "34",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Edith Roriguez"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "35",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Alejandra Botello"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "36",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Walter Reimers"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "37",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Noemi Botello Hurtado"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "38",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["David Hurtado"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "39",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Bernardo Hurtado"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "40",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Carlos Ponce"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "41",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Oscar Mazatán"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "42",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Julia García"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "43",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Andrea Márquez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "44",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Miguel Ramos"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "45",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Marcos Castillo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "46",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Antonio Ramírez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "47",
      nombre   : "Noemi Botello Hurtado",
      invitados: ["Enrique Botello"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "48",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Ivan Ontiveros"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "49",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Lucia de la Rocha"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "50",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Liliana Rabago "],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "51",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Lizeth de la Rocha"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "52",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Ana Celia Alcalá"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "53",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Ubaldo López"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "54",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Magdalena Carrillo "],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "55",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Alejandro Méndez"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "56",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Vianney Robles"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "57",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Ivana Ontiveros"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "58",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Jorge Ontiveros"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "59",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Mary Ontiveros"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "60",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Ana Paula Ramos"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "61",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["María José Ontiveros"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "62",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Baudelia Rodríguez"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "63",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Alex Díaz"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "64",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Regina Murillo"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "65",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Oscar Perales"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "66",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Ubaldo López Rabago"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "67",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Ana Lucia Ontiveros de la Rocha"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "68",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Oscar Perales"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "69",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Mario Hernández"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "70",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Victoria de la Torre"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "71",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Andrea Escareño"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "72",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Max de la Torre"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "73",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Paulina García"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "74",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Ximena López"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "75",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Emilio Angulo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "76",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Diego Sánchez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "77",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Alejandro Díaz"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "78",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Daniel Ortíz"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "79",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Rafael Candelas"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "80",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Alejandro Elizondo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "81",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Angela Enciso"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "82",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Isaac Ortíz"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "83",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Javier Hernández"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "84",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Alejandro Adame"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "85",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Cristina López"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "86",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Julieta Vargas"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "87",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Nicole Ramírez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "88",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Valentina Romo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "89",
      nombre   : "Ana Lucia Ontiveros de la Rocha",
      invitados: ["Bernardo Caraza"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "90",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Gabriela González"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "91",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Antonio Huerta"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "92",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Ana Huerta"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "93",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Ana Paola Calderón"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "94",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Gabriela Huerta"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "95",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["David Huerta"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "96",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Martha Sánchez"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "97",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Rodolfo González"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "98",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Rodrigo González"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "99",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Diego Borda"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "100",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Aly Daniela Huerta González"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "101",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Omar Adrian Martínez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "102",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Germán Ruíz"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "103",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Paloma Valdez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "104",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Paulina Cordero"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "105",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Susana Mejia"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "106",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Alejandro Morales"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "107",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Juan Pablo Trejo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "108",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Alexa Pichardo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "109",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Cristobal Márquez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "110",
      nombre   : "Aly Daniela Huerta González",
      invitados: ["Daniel Contreras"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "111",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Catalina Mendoza García"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "112",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Claudia Castillo Mendoza"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "113",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Eduardo Castillo Mendoza"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "114",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Alma karina González "],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "115",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Claudia Fernanda Castillo"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "116",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Eddy Alejandro Castillo"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "117",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["María Renata Castillo"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "118",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Fátima Castillo Becerril"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "119",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Camila Castillo González"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "120",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Arturo Castillo González"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "121",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Maria del Regugio de la Rosa"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "122",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Lorena Santoyo de la Rosa"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "123",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Zeus Santoyo de la Rosa"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "124",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Estela Cardenas Vargas"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "125",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Ricardo Damian Jacinto Cuevas"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "126",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Jhonattan Daniel Castro P."],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "127",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Sergio Francisco Mendoza E."],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "128",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Ximena Santoyo Castillo"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "129",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Dana Monreal"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "130",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Vanessa Nhle Campos"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "131",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Alejandra Arguelles"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "132",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Regina Sánchez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "133",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Pablo Díaz"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "134",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Raúl González"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "135",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Thelma Báez Rosso"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "136",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Ana Paulina Bañuelos Robles"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "137",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Andrea Estrada Reyes"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "138",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Karina Acosta Mares"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "139",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Katya Acosta Mares"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "140",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Anthony Evan Ramíez Rangel"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "141",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Jorge Ramírez Cassale"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "142",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Paola Santoyo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "143",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Shamira Martínez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "144",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Camila Ortega Inguanzo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "145",
      nombre   : "Ximena Santoyo Castillo",
      invitados: ["Enrique Sigg"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "146",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Sergio Soriano Flores"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "147",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Patricia Báez Medina"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "148",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Santiago Soriano B."],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "149",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Annabel Juárez V."],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "150",
      nombre   : "Patricia Soriano Báez",
      invitados: ["José Báez Medina"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "151",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Mónica Zevada Anderson"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "152",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Daniela Báez"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "153",
      nombre   : "Patricia Soriano Báez",
      invitados: ["José Pablo Báez"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "154",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Veronica Báez P."],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "155",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Juan Manuel Pinto"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "156",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Efrain Flores Báez"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "157",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Victor González"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "158",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Analia Soriano F."],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "159",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Gabriel Aldeco A."],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "160",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Celia Medina Díaz"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "161",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Gonzalo Mier"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "162",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Ana Ruelas"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "163",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Patricia Soriano Báez"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "164",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Felipe Martínez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "165",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Yaco Reimers"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "166",
      nombre   : "Patricia Soriano Báez",
      invitados: ["José María Inguanzo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "167",
      nombre   : "Patricia Soriano Báez",
      invitados: ["German Inguanzo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "168",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Chema Hinostroza"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "169",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Fabio Ortiz"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "170",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Creta Macias"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "171",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Ivanna Caraza"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "172",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Anapaula Díaz"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "173",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Mauricio Díaz"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "174",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Andrea Casas"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "175",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Ana Ramírez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "176",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Estrella Abarca"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "177",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Emiliano Ponce"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "178",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Rodrigo Delgado"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "179",
      nombre   : "Patricia Soriano Báez",
      invitados: ["José Pablo Márquez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "180",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Rebeca Arellano"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "181",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Eduardo Pérez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "182",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Mariano Argüelles"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "183",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Eduardo Reyes"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "184",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Jesus Abarca"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "185",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Checo Lamadrid"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "186",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Rocio Gonzalez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "187",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Fernando Ruiz"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "188",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Mauricio Mazatan"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "189",
      nombre   : "Patricia Soriano Báez",
      invitados: ["César Ortiz"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "190",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Sebastian de Alba"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "191",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Rubén Padilla"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "192",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Jorge Valdés"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "193",
      nombre   : "Patricia Soriano Báez",
      invitados: ["Jorge Romo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "194",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Francisco Jávier Pérez"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "195",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Beatriz Borrego"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "196",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Regina Pérez Borrego"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "197",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Ana Sofia Pérez Borrego"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "198",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Javier Pérez Borrego"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "199",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Santiago Borrego"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "200",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Mariana Borrego"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "201",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Manuel Pérez"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "202",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Francisco Pérez"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "203",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Santiago Pérez"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "204",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Ximena Contreras"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "205",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Pablo Borrego"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "206",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Fernanda Borrego"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "207",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Andrea Borrego"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "208",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Jose Antonio Llaguno"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "209",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Ricky Estrada"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "210",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Tasaki Kusulas"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "211",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Leticia Escobedo"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "212",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Fernando Argüelles"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "213",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Jocelyne Mazatan"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "214",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Pía Pérez Borrego"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "215",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Andrea Alba"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "216",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Gonzalo Enciso"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "217",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Eduardo Jáquez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "218",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Fernando Soto"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "219",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Diego Becerra"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "220",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Alejandra Jáquez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "221",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Benjamin Jáquez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "222",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Vale Fuenzalida"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "223",
      nombre   : "Pía Pérez Borrego",
      invitados: ["Majo Escareño"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "224",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Yair Miranda"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "225",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Enrique Flores"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "226",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Ana Lucia Flores"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "227",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Jesús Medina"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "228",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Giovanna Salmón"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "229",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Enrique Flores"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "230",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Lucy Medina"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "231",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Lucia Mazzocco"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "232",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Daianna Salmón"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "233",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Carlos Salmón"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "234",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Kareli Sandoval"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "235",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Daianna Buenrostro"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "236",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Daniela Buenrostro"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "237",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Jesús Medina"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "238",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["José Pablo Medina"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "239",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Ana Paula Medina Salmón"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "240",
      nombre   : "Ana Paula Medina Salmón",
      invitados: [""],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "241",
      nombre   : "Ana Paula Medina Salmón",
      invitados: [""],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "242",
      nombre   : "Ana Paula Medina Salmón",
      invitados: [""],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "243",
      nombre   : "Ana Paula Medina Salmón",
      invitados: [""],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "244",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Santiago Ortíz"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "245",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Luis Velasco"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "246",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Mariana Ferreira"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "247",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Luis Ángulo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "248",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Maureen Piña"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "249",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Ana Romo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "250",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Vale Cassale"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "251",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Amanda Rosso"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "252",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Mauricio Pastor"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "253",
      nombre   : "Ana Paula Medina Salmón",
      invitados: ["Natalia Mendieta"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "254",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Samantha Saucedo"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "255",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Camila Celaya"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "256",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Sofia Carrillo"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "257",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Dana Cacahuatitla"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "258",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Pamela de la Rosa"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "259",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Omar Celaya"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "260",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Ricardo de la Rosa"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "261",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Monserrat Chairez"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "262",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Raúl Celaya"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "263",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Ma.Elena de la Rosa"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "264",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Sonia Celaya"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "265",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Gloria Pulido"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "266",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Raúl Morales"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "267",
      nombre   : "Martin Pulido Celaya",
      invitados: ["David Pulido"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "268",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Martha Pulido"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "269",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Pablo Pulido"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "270",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Sarai de la Torre"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "271",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Martin Pulido"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "272",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Homero"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "273",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Gabriela Pulido"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "274",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Gloria Pulido"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "275",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Pablo Hernández"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "276",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Ivone de la Rosa"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "277",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Claudia de la Rosa"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "278",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Martin Pulido Celaya"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "279",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Sebastian Llaguno"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "280",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Bernardo Argúelles"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "281",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Cinthia Murillo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "282",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Ale Romo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "283",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Julio Avila"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "284",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Kevin Tello"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "285",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Amairani Sánchez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "286",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Ana Anaya"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "287",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Carmita Forstner"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "288",
      nombre   : "Martin Pulido Celaya",
      invitados: ["Alfonso Canseco"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "289",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Mamina"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "290",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Quita"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "291",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Gerardo Reyes"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "292",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Felos Fernández"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "293",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Sofia Reyes"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "294",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Juliana Reyes Fernández"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "295",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Nicole Ramírez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "296",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Mariana Contreras"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "297",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Paulina Carrera"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "298",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Carlos Bañuelos"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "299",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Valentino de la Torre"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "300",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Fernanda Valverde"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "301",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Regina de la Torre"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "302",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Natalia Pastor"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "303",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Diero Reimers"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "304",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Manuel Reyes"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "305",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Roberto Acuña"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "306",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Chuy Romo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "307",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Mau Mier"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "308",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Alessa Siller"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "309",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Tiara Chairez"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "310",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Yailin Izar"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "311",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Natalia Pichardo"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "312",
      nombre   : "Juliana Reyes Fernández",
      invitados: ["Jose Luis Talavera"],
      mesa   : 1,
      horario: "10:30"
    },
    {
      codigo : "313",
      nombre   : "Maestros",
      invitados: ["Miss Nuria Frausto"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "314",
      nombre   : "Maestros",
      invitados: ["Miss Laura Frausto"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "315",
      nombre   : "Maestros",
      invitados: ["Miss Alejandra Jáquez"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "316",
      nombre   : "Maestros",
      invitados: ["Profe Joel"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "317",
      nombre   : "Maestros",
      invitados: ["Profe Felipe"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "318",
      nombre   : "Maestros",
      invitados: ["Miss Connie Davinson"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "319",
      nombre   : "Maestros",
      invitados: ["Profe Gabriel Santamaria"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "320",
      nombre   : "Maestros",
      invitados: ["Profe Ivan"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "321",
      nombre   : "Maestros",
      invitados: ["Padre Jose Antonio Guzmán"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "322",
      nombre   : "Maestros",
      invitados: ["Miss Vero Galicia"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "323",
      nombre   : "Maestros",
      invitados: ["Miss Lupita"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "324",
      nombre   : "Maestros",
      invitados: ["Profe Paco Carreón"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "325",
      nombre   : "Maestros",
      invitados: ["Miss Arlette"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "326",
      nombre   : "Maestros",
      invitados: ["Miss Cecilia Enciso"],
      mesa   : 1,
      horario: "9:00"
    },
    {
      codigo : "327",
      nombre   : "Maestros",
      invitados: ["Miss Lula Enciso"],
      mesa   : 1,
      horario: "9:00"
    }
    
  ];

  constructor(private rutaActiva: ActivatedRoute) { }

  ngOnInit() {
    try {
      //console.table(this.graduados);
      this.codigoUrl = {
        codigo: this.rutaActiva.snapshot.params.codigo
      };

      //Metodo para comparar el código que se manda por la url y agregar la información en la plantilla
      this.rutaActiva.params.subscribe(
        (params: Params) => {
          this.codigoUrl.codigo = params.codigo;
            for (let clave of this.graduados){
              //console.log(clave.codigo);
              if(params.codigo  === clave.codigo){
                this.nombre = clave.nombre;
                this.invitados = clave.invitados;
                this.mesa = clave.mesa;
                this.horario = clave.horario;
                
                this.encontrado=1;
                break;
              }
            }if (this.encontrado==0) {
              Swal.fire({
                title: "Código no valido",
                text: "Código no valido",
                icon: "error"
              }).then((result) => {
               
                
                setTimeout (() => {
                  window.location.reload();
                
                }, 1500);
                
              });
            }
           
        }
      );
    } catch (error) {
      console.log(error);
    }
    
  }
}

