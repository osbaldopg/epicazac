import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GraduadosComponent } from './graduados/graduados.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    GraduadosComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: 'graduados', component: GraduadosComponent},
      {path: 'graduados/:codigo', component: GraduadosComponent}
      
    ]),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
